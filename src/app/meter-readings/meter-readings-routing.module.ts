// Core Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { MeterHomeComponent } from './meter-home.component';


const routes: Routes = [
  {
    path: '',
    component: MeterHomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeterReadingsRoutingModule { }
