// Core Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing Modules
import { MeterReadingsRoutingModule } from './meter-readings-routing.module';

// Components
import { DensityComponent } from './meters/density/density.component';
import { MeterHomeComponent } from './meter-home.component';
import { PressureComponent } from './meters/pressure/pressure.component';


@NgModule({
  declarations: [
    DensityComponent,
    MeterHomeComponent,
    PressureComponent
  ],
  imports: [
    CommonModule,
    MeterReadingsRoutingModule
  ]
})
export class MeterReadingsModule { }
