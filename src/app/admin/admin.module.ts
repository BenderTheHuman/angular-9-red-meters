// Core Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { AdminRoutingModule } from './admin-routing.module';

// Componenets
import { AdminHomeComponent } from './admin-home.component';
import { ProfileComponent } from './admin-components/profile/profile.component';
import { SettingsComponent } from './admin-components/settings/settings.component';


@NgModule({
  declarations: [
    AdminHomeComponent,
    ProfileComponent,
    SettingsComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
