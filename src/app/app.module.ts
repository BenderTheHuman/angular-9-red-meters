// Core Modules
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { JwtModule } from '@auth0/angular-jwt';

// Routing
import { AppRoutingModule } from './app-routing.module';

// Custom Modules
import { AdminModule } from './admin/admin.module';
import { MeterReadingsModule} from './meter-readings/meter-readings.module';
import { UserModule } from './user/user.module';

// Imported Modules
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

// Components
import { AppComponent } from './app.component';

// Services
import { DensityService } from './_services/density.service';
import { PressureService } from './_services/pressure.service';

// Environment Variables
import { environment } from '../environments/environment';

const config: SocketIoConfig = { url: environment.socketUrl, options: {} };

export function tokenGetter() {
  return localStorage.getItem("red-meter-jwt");
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdminModule,
    HttpClientModule,
    MeterReadingsModule,
    UserModule,
    SocketIoModule.forRoot(config),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:3000'],
        blacklistedRoutes: ['http://localhost:3000/auth/login']
      }
    })
  ],
  providers: [
    DensityService,
    PressureService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
