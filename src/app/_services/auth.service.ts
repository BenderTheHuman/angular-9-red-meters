// Core Modules
import { Injectable } from '@angular/core';

// Services
import { AuthDataService } from './auth-data.service';

// Models
import { AuthGroup } from '../_models/auth.types';


@Injectable()
export class AuthorizationService {

    permissions: Array<string>; // Store the actions for which this user has permission

    constructor(private authDataService: AuthDataService) { }

    hasPermission(authGroup: AuthGroup) {
        if (this.permissions && this.permissions.find(permission => {
                return permission === authGroup;
            })) {
            return true;
        }
        return false;
    }

    // This method is called once and a list of permissions is stored in the permissions property
    initializePermissions() {
        return new Promise((resolve, reject) => {
            // Call API to retrieve the list of actions this user is permitted to perform. (Details not provided here.)
            // In this case, the method returns a Promise, but it could have been implemented as an Observable
            this.authDataService.getPermissions()
                .then(permissions => {
                    this.permissions = permissions;
                    resolve();
                })
                .catch((e) => {
                    reject(e);
                });
        });
    }
}
