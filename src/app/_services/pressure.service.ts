// Core Modules
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

// Imported Modules
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class PressureService {

  constructor(private socket: Socket) { }

  public sendPressureMessage(message) {
    this.socket.emit('to-pressure-api-message', message);
  }

  public getPressureReadings = () => {
    return new Observable((observer) => {
            this.socket.on('pressure-reading', (reading) => {
                observer.next(reading);
            });
    });
  }
}
