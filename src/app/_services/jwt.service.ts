// Core Modules
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string) {
    return this.http.post<{access_token: string}>(`http://${url}/auth/login`,
      {email, password})
      .pipe(tap(res => {
        localStorage.setItem('red-meter-jwt', res.access_token);
      }));
  }

  logout() {
    localStorage.removeItem('red-meter-jwt');
  }

  public get loggedIn(): boolean {
    return localStorage.getItem('red-meter-jwt') !==  null;
  }

  // register(email: string, password: string) {
  //   return this.http.post<{access_token: string}>(`http://${url}/auth/register`,
  //     {email, password})
  //     .pipe(tap(res => {
  //       this.login(email, password);
  //     }));
  // }
}
