// Core Modules
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

// Imported Modules
import { Socket } from 'ngx-socket-io';


@Injectable({
  providedIn: 'root'
})
export class DensityService {

  constructor(private socket: Socket) { }

  public sendDensityMessage(message) {
    this.socket.emit('to-density-api-message', message);
  }

  public getDensityReadings = () => {
    return new Observable((observer) => {
            this.socket.on('density-reading', (reading) => {
                observer.next(reading);
            });
    });
  }
}
