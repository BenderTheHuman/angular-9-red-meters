// Core Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { UserRoutingModule } from './user-routing.module';

// Components
import { ProfileComponent } from './user-components/profile/profile.component';
import { SettingsComponent } from './user-components/settings/settings.component';
import { UserHomeComponent } from './user-home.component';

@NgModule({
  declarations: [
    ProfileComponent,
    SettingsComponent,
    UserHomeComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
